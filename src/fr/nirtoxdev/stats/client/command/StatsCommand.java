package fr.nirtoxdev.stats.client.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.nirtoxdev.stats.Core;
import fr.nirtoxdev.stats.client.gui.MainGUI;

public class StatsCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		boolean valid = false;
		
		if(!(sender instanceof Player)){
			sender.sendMessage("Seul les joueurs peuvent executes cette commande.");			
			return false;
		}
		
		Player player = (Player) sender;
		
		if(args.length == 0){
			Bukkit.getScheduler().runTaskLater(Core.get(), new Runnable() {
				
				@Override
				public void run() {
					player.openInventory(MainGUI.get().getPlayerPage(player.getName()));
				}
			}, 5);
			
			valid = true;
		}else if(args.length == 1){
			if(args[0].equalsIgnoreCase("help")){
				sendHelpMessages(player);
				
				valid = true;
			}else{
				Player target = Bukkit.getPlayer(args[0]);
				
				if(target != null){
					Bukkit.getScheduler().runTaskLater(Core.get(), new Runnable() {
						
						@Override
						public void run() {
							player.openInventory(MainGUI.get().getPlayerPage(target.getName()));
						}
					}, 5);
					
					valid = true;
				}else{
					player.sendMessage("�cJoueur d�connect�");
					valid = true;
				}
			}
		}
		
		if(!valid){
			sendHelpMessages(player);
			return false;
		}else{
			return true;
		}
	}

	public void sendHelpMessages(Player player){
		String[] help = new String[4];
		
		help[0] = ("�b------------- �lAide �b-------------");
		help[1] = ("�f/stats - �6Vos statistiques");
		help[2] = ("�f/stats <player> - �6Statistiques d'un joueur");
		help[3] = ("�f/stats help - �6Page d'aide");
		
		player.sendMessage(help);
	}
}
