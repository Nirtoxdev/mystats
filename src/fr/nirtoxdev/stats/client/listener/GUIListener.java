package fr.nirtoxdev.stats.client.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;

public class GUIListener implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent event){
		if(!(event.getWhoClicked() instanceof Player)) return;
				
		Inventory inventory = event.getClickedInventory();
		Player player = (Player) event.getWhoClicked();
		
		if(inventory != null && ChatColor.stripColor(inventory.getName()).startsWith("Stats de")){
			event.setCancelled(true);
			
			ItemStack item = event.getCurrentItem();
			
			if(item != null && item.hasItemMeta() && item.getItemMeta().hasDisplayName() 
					&& ChatColor.stripColor(item.getItemMeta().getDisplayName()).equalsIgnoreCase("[X] Fermer")){
				player.closeInventory();
			}
		}
	}
}
