package fr.nirtoxdev.stats.client.listener;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import fr.nirtoxdev.stats.client.PlayerStat;
import fr.nirtoxdev.stats.client.PlayerStat.StatType;

public class StatListener implements Listener {
	
	Map<String, Integer> streaks = new HashMap<>();
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event){
		if(!(event.getEntity() instanceof Player)) return;
		
		Player player = event.getEntity();
		
		int player_score = PlayerStat.get().getScore(player.getName(), StatType.DEATH);
		
		PlayerStat.get().updateScore(player.getName(), StatType.DEATH, player_score+1);
		
		if(player.getKiller() != null && player.getKiller() instanceof Player){
			Player killer = player.getKiller();
			int killer_score = PlayerStat.get().getScore(killer.getName(), StatType.KILL);
			
			PlayerStat.get().updateScore(killer.getName(), StatType.KILL, killer_score+1);
		}
	}

}
