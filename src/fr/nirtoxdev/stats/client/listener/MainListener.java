package fr.nirtoxdev.stats.client.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.nirtoxdev.stats.client.PlayerStat;
import fr.nirtoxdev.stats.server.Stats;

public class MainListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
				
		Stats.get().initializeAccount(player.getName());			
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event){
		Player player = event.getPlayer();
		
		PlayerStat.get().updateScoreToDB(player.getName());
		Stats.get().users.remove(player.getName());
	}
}
