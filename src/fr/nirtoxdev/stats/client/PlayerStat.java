package fr.nirtoxdev.stats.client;

import java.util.HashMap;
import java.util.Map;

import fr.nirtoxdev.stats.server.Stats;

public class PlayerStat {
	
	private static PlayerStat instance;
	
	public static PlayerStat get(){
		if(instance == null) instance = new PlayerStat();
		
		return instance;
	}
	
	public void updateScore(String playerName, StatType type, int newScore){
		type.getList().replace(playerName, newScore);
	}
	
	public int getScore(String playerName, StatType type){
		return type.getList().get(playerName);
	}
	
	public void updateScoreToDB(String playerName){
		Stats stats = Stats.get();
		
		for(StatType type: StatType.values()){
			int score = type.getList().get(playerName);
			
			stats.updateStatistic(playerName, type, score);
		}
	}
	
	public void addToLists(String playerName){
		for(StatType type: StatType.values()){
			int score = Stats.get().getStatistic(playerName, type);
			
			type.getList().put(playerName, score);
		}
	}

	public enum StatType {

		KILL("kills"), DEATH("deaths"), STREAK("kill_streaks"), TIME("timePlayed");

		private String key;
		
		Map<String, Integer> kills = new HashMap<>();
		Map<String, Integer> deaths = new HashMap<>();
		Map<String, Integer> streaks = new HashMap<>();
		Map<String, Integer> playerTimes = new HashMap<>();
		
		StatType(String key){
			this.key = key;
		}
		
		public String getKey(){
			return key;
		}

		public Map<String, Integer> getList(){
			switch (this) {
			case KILL:
				return kills;
			case DEATH:
				return deaths;
			case STREAK:
				return streaks;
			case TIME:
				return playerTimes;
			default:
				return null;
			}

		}
	}
}
