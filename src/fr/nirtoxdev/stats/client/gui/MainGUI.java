package fr.nirtoxdev.stats.client.gui;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.nirtoxdev.stats.client.PlayerStat;
import fr.nirtoxdev.stats.client.PlayerStat.StatType;

public class MainGUI {
	
	private static MainGUI instance;
	
	public static MainGUI get(){
		if(instance == null) instance = new MainGUI();
		
		return instance;
	}
	
	public Inventory getPlayerPage(String playerName){
		Inventory inventory = Bukkit.createInventory(null, 9, "�6Stats de "+playerName);
		
		ItemStack kills = new ItemStack(Material.IRON_SWORD);
		ItemMeta killsM = kills.getItemMeta();
		killsM.setDisplayName("�4Meurtres");
		killsM.setLore(Arrays.asList("�e"+PlayerStat.get().getScore(playerName, StatType.KILL)+" meurtres !"));
		kills.setItemMeta(killsM);
		
		inventory.setItem(3, kills);
		
		ItemStack deaths = new ItemStack(Material.SPIDER_EYE);
		ItemMeta deathsM = deaths.getItemMeta();
		deathsM.setDisplayName("�cMorts");
		deathsM.setLore(Arrays.asList("�e"+PlayerStat.get().getScore(playerName, StatType.DEATH)+" morts"));
		deaths.setItemMeta(deathsM);
		
		inventory.setItem(4, deaths);
		
		@SuppressWarnings("deprecation")
		ItemStack time = new ItemStack(Material.getMaterial(347));
		ItemMeta timeM = time.getItemMeta();
		timeM.setDisplayName("�6Temps jou�");
		timeM.setLore(Arrays.asList("�e"+PlayerStat.get().getScore(playerName, StatType.TIME)+" minutes"));
		time.setItemMeta(timeM);
		
		inventory.setItem(5, time);
		
		ItemStack close = new ItemStack(Material.REDSTONE);
		ItemMeta closeM = close.getItemMeta();
		closeM.setDisplayName("�c[X] Fermer");
		close.setItemMeta(closeM);
		
		inventory.setItem(8, close);
		
		return inventory;
	}
}
