package fr.nirtoxdev.stats.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import fr.nirtoxdev.stats.Core;
import fr.nirtoxdev.stats.client.PlayerStat;
import fr.nirtoxdev.stats.client.PlayerStat.StatType;
import paulek.mysql.MySQL;

public class Stats {
	
	private static Stats instance;
	private Logger logger = Core.get().getLogger();
	public MySQL SQL;
    public boolean connected;
	
    public Map<String, Integer> users = new HashMap<>();
    
	public static Stats get(){
		if(instance == null){
			instance = new Stats();
		}
		
		return instance;
	}
	
	public void initializeAccount(String playerName){
		if(!isRegister(playerName)){
			registerUser(playerName);
		}
		
		PlayerStat.get().addToLists(playerName);
	}
	
	
	
	
	
	
	public void registerUser(String playerName){
		SQL.update(String.format("INSERT INTO users (pseudo) VALUES ('%s')", playerName));
		int id = getPlayerId(playerName);

		SQL.update(String.format("INSERT INTO stats (user_id) VALUES ('%s')", id));

	}
	
	public boolean isRegister(String playerName){
		final ResultSet rs = SQL.query(String.format("SELECT user_id FROM users WHERE pseudo = '%s'", playerName));
		boolean registered = false;
		
		try{			
		    while(rs.next()){
		    	registered = true;
		    	
		    	users.put(playerName, rs.getInt("user_id"));
		    	
		    	rs.close();
		    }
		}catch (SQLException e) {
			logger.info("Une requete sql a echoue: " + e.getErrorCode());
		}
		
		return registered;
	}
	
	
	
	
	
	
	
	
	public int getPlayerId(String playerName){
		if(users.containsKey(playerName)){
			return users.get(playerName);
		}else{
			final ResultSet rs = SQL.query(String.format("SELECT user_id FROM users WHERE pseudo = '%s'", playerName));
			int id = -1;
			
			try{			
			    while(rs.next()){
			    	id = rs.getInt("user_id");
			    	users.put(playerName, id);
			    	
			    	rs.close();
			    }
			}catch (SQLException e) {
				logger.info("Une requete sql a echoue: " + e.getErrorCode());
			}
			
			return id;
		}
	}
	
	public int getStatistic(String playerName, StatType type){
		int id = getPlayerId(playerName);
		final ResultSet rs = SQL.query(String.format("SELECT "+type.getKey()+" FROM stats WHERE user_id = '%s'", id));
		int time = 0;
		
		try{			
		    while(rs.next()){
		    	time = rs.getInt(type.getKey());
		    	rs.close();
		    }
		}catch (SQLException e) {
			logger.info("Une requete sql a echoue: " + e.getErrorCode());
		}
		
		return time;
	}
	
	public void updateStatistic(String playerName, StatType type, int newStat){
		int id = getPlayerId(playerName);

		SQL.update(String.format("UPDATE stats SET "+type.getKey()+" = '%d' WHERE user_id = '%s'", newStat, id));
	}
}
