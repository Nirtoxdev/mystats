package fr.nirtoxdev.stats;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.nirtoxdev.stats.client.PlayerStat;
import fr.nirtoxdev.stats.client.PlayerStat.StatType;
import fr.nirtoxdev.stats.client.command.StatsCommand;
import fr.nirtoxdev.stats.client.listener.GUIListener;
import fr.nirtoxdev.stats.client.listener.MainListener;
import fr.nirtoxdev.stats.client.listener.StatListener;
import fr.nirtoxdev.stats.server.Stats;
import paulek.mysql.Main;
import paulek.mysql.MySQL;

public class Core extends JavaPlugin {
	
	private static Core instance;
	private Stats stats;
	private FileConfiguration config = getConfig();
	private int timeupdatetoDB;


	@Override
	public void onEnable() {
		instance = this;
		stats = Stats.get();
				
		stats.SQL = new MySQL((Main) Bukkit.getPluginManager().getPlugin("MySQL"), getDescription().getName());
		stats.connected = stats.SQL.Connect(config.getString("connection.host"), config.getString("connection.database"), config.getString("connection.user"), config.getString("connection.password")); // CONFIGURABLE VALUES

        if(!stats.connected){
        	getLogger().severe("Erreur pendant la connexion au serveur MySQL");
        }
        
        initializeConfig();
        
		registerListeners();
		registerCommands();
		
		Bukkit.getScheduler().runTaskLater(this, new Runnable() {
			
			@Override
			public void run() {
				loopUpdateTimePlayed(); // WAIT 30 SECONDS BEFORE LAUNCHING THE LOOP
				loopUpdateScores(); 
			}
		}, 600);
	}
	
	public static Core get(){
		return instance;
	}
	
	private void initializeConfig(){
	    config.addDefault("connection.host", "127.0.0.1");
	    config.addDefault("connection.database", "minecraft");
	    config.addDefault("connection.user", "root");
	    config.addDefault("connection.password", "password");
	    config.addDefault("timeupdatetoDB", 1200);
	    config.options().copyDefaults(true);
	    saveConfig();
	    
	    timeupdatetoDB = config.getInt("timeupdatetoDB");
	}
	
	private void registerListeners(){
		PluginManager pm = Bukkit.getPluginManager();
		
		pm.registerEvents(new MainListener(), this);
		pm.registerEvents(new GUIListener(), this);
		pm.registerEvents(new StatListener(), this);
	}
	
	private void registerCommands(){
		getCommand("stats").setExecutor(new StatsCommand());
	}
	
	private void loopUpdateScores(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			
			@Override
			public void run() {
				for(String playerName : Stats.get().users.keySet()){
					PlayerStat.get().updateScoreToDB(playerName);
				}
			}
		}, 20, timeupdatetoDB); // CONFIGURABLE TIME
	}
	
	private void loopUpdateTimePlayed(){
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			
			@Override
			public void run() {
				for(String playerName : Stats.get().users.keySet()){
					int score = PlayerStat.get().getScore(playerName, StatType.TIME);
					
					PlayerStat.get().updateScore(playerName, StatType.TIME, score+1);
				}
			}
		}, 0, 1200);
	}
}
